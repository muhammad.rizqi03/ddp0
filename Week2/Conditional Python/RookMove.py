# Asking location
first_column = int(input())
first_row = int(input())

# Asking second cell
sec_column = int(input())
sec_row = int(input())


# Jika memenuhi kondisional ini, berarti rook bisa bergerak. Selain itu rook tidak bisa bergerak
if first_column == sec_column or first_row == sec_row:
    print("YES")
else:
    print("NO")