# Read an int
a = int(input())
# Menyimpan input sebagai nilai terkecil sementara
smallest_num = a
# Using for loop untuk minta input dan merubah nilai terkecil jika ada yg lebih kecil
for i in range(2):
    b = int(input())
    if b < smallest_num :
        smallest_num = b
print(smallest_num)