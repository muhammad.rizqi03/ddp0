'''
Idenya: untuk input selain 0 dan 1, akan dilakukan looping sebanyak n - 1 kali untuk menambahkan
        2 nilai sebelumnya
'''

# Asking input
n = int(input())

# Untuk input 0 langsung print 0
if n == 0:
    print(0)
# Untuk input 1, ga akan menjalankan loopnya dan langsung print b yg bernilai 1
# Selain 2 input itu, akan looping. Di setiap loop nya, akan memindahkan nilai b ke a, dan 
# mengubah nilai b menjadi hasil penjumlahan nilai a dan b sebelum nilai a dan b berubah.
else:
    a, b = 0, 1
    for i in range(2, n + 1):
        a, b = b, a + b
    print(b)