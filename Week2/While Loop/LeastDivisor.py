# Asking input
inp_num = int(input())
num = 2

# Looping selama nilai inp_num modulo num tidak bernilai 0
# Atau selama nilai num bukan merupakan integer divisor dari input
while (inp_num % num) != 0:
    num += 1
print(num)