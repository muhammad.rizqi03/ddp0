#Asking input
input_num = int(input())

#Integer number
int_num = 1

#Looping selama nilai kuadrat dari int_num masih kurang dari input
while int_num ** 2 <= input_num:
    print(int_num**2, end=" ")
    int_num += 1
    
#Parameter end pada print dipakai agar print output dalam 1 baris