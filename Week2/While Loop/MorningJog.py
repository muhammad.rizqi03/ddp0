# Asking input
first_miles = int(input())
target_miles = int(input())

day = 1

# Looping selama miles yg dicapai masih kurang dari target nya
# Di setiap loop miles yg dicapai meningkat 10% atau 0.1 dari sebelumnya
while first_miles < target_miles:
    first_miles += first_miles * 0.1
    day += 1

print(day)