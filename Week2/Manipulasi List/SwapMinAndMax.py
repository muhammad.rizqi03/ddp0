# Asking input
a = [int(i) for i in input().split()]

# Find the max and min int in list
# function max dan min dipakai untuk mencari nilai maksimum dan minimum elemen pada sebuah list
max_int = max(a)
min_int = min(a)

# Find the location / index of both int
# method index() dipakai untuk mencari index sebuah elemen dari sebuah list
index_max = a.index(max_int)
index_min = a.index(min_int)

# Swap them
a[index_max] = min_int
a[index_min] = max_int

for elm in a:
    print(elm)