# Asking input
list_of_nums = input().split()

# Looping sebanyak elemen pada list_of_nums - 1
# Print jika terdapat nilai yg lebih besar dari nilai pada indeks sebelumnya
for i in range(1, len(list_of_nums)):
    if int(list_of_nums[i]) > int(list_of_nums[i - 1]) :
        print(list_of_nums[i])
    