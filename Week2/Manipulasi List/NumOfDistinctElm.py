# Asking input
'''
Tipe input dibawah ini disebut list comprehension, 
penjelasan simplenya list comprehension itu melakukan looping dan 
menjadikan elemen pada setiap loop tsb sebagai elemen pada sebuah list 
'''

a = [int(i) for i in input().split()]
b = []

# Melakukan looping dan menambahkan elm yg belum ada di list b
for elm in a:
    if not elm in b:
        b.append(elm)
        
print(len(b))

# Sebenernya bisa pake set aja biar lebih simple, tapi karena diminta nya pake list, jadi gini