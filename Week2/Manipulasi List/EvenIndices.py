# Minta input, lalu menggunakan method split untuk dijadikan list
# Method split digunakan untuk memisahkan karakter input dengan parameter yg ada di dalamnya menjadi list
list_of_num = input().split()

# Looping dan print elemen yg memiliki indeks genap
for i in range(len(list_of_num)):
    if i % 2 == 0 :
        print(list_of_num[i], end=" ")