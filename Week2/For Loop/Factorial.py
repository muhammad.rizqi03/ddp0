#Read an integer
n = int(input())
result = 1
#looping sebanyak n kali dan mengalikan nilai i dengan result, agar menjadi perhitungan faktorial
for i in range(1, n + 1):
    result *= i
print(result)