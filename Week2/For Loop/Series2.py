#Read integer
A = int(input())
B = int(input())
#Print A to B inclusively in ascending order if A < B
if A < B:
    for i in range(A, B + 1):
        print(i)
#Print A to B inclusively in descending order if A >= B
else:
    for i in range(A, B - 1, -1):
        print(i)