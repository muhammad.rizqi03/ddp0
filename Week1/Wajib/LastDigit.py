'''
Penjelasan : 
terdapat beberapa cara untuk melakukan ini diantaranya : 
1. Dengan melakukan pembagian bersisa 
2. Dengan melakukan string manipulasi
'''


#Cara 1
number = int(input())
print(number % 10) # akan menghasilkan last digit

#Cara 2
number = input()
print(number[-1]) # akan menunjukan karakter terakhir