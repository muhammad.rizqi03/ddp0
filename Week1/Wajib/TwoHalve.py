'''
Penjelasan : 
Kita slice dari ceil(n/2) s/d n
lalu kita gabungkan dengan slice dari 0 s/d ceil(n/2) - 1
'''

import math # Untuk bisa menggunakan fungsi Ceil
a = input()
n = len(a)
x = math.ceil(n/2)
print(a[x:]+a[:x])