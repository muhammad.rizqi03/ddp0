'''
Penjelasan : 
outputkan sesuai soal
'''
a = input() # Misal n = panjang string a
print(a[2]) #Karakter ke-3
print(a[-2]) # Karakter 2 terakhir
print(a[:5]) # Karakter 0 s/d 5
print(a[:-2]) # Karakter 0 s/d (n - 3) inklusif
print(a[::2]) # Karakter 0 s/d n , dengan loncatan 2
print(a[1::2]) #Karakter 2 s/d n,  dengan loncatan 2
print(a[::-1]) #Karakter n s/d 0 (Alur mundur)
print(a[::-2]) # Karakter n s/d 0 (Alur mundur), dengan loncatan 2
print(len(a)) # Output panjang string