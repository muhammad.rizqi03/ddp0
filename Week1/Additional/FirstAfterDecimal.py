'''
URL : https://snakify.org/en/lessons/integer_float_numbers/problems/digit_after_decimal_point/
Penjelasan : 
Ubah float menjadi string, lalu cari terlebih dahulu angka koma menggunaka fungsi find
'''
number = input()
koma = number.find(".",0,len(number))
print(number[koma + 1])