'''
URL : https://snakify.org/en/lessons/strings_str/problems/num_words/
Penjelasan : 
Hal ini menggunakan fungsi - fungsi tertentu, bisa melihat dokumentasi python untuk lebih jelasnya
Dan terdapat materi advance yakni set untuk mengerjakan hal ini
'''
string = input().split() #untuk mengubah menjadi array
string = set(string) #Untuk mengubah menjadi set
print(len(string))