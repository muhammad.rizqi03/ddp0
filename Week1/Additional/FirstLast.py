'''
URL : https://snakify.org/en/lessons/strings_str/problems/first_and_last_occurrences/https://snakify.org/en/lessons/strings_str/problems/first_and_last_occurrences/
Penjelasan : 
Apabila huruf f hanya ada 1 maka kita  tidak perlu untuk mncari f terakhir.
Menggunakan slicing dan fungsi find 
'''
input = input()
end = input.find("f",0,len(input))
input = input[::-1]
start = input.find("f",0,len(input))
print(start, end)
